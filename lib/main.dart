import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';

void main() {
  // TODO do we need to configure things?
  runApp(const MyApp());
}

typedef Recipe = String;

// This is owned by the dart worker I think
class StateRecipes extends ChangeNotifier {
  List<Recipe> textList = [
    "Hühnerfrikassee",
    "Nicht Hühnerfrikassee",
  ];

  addRecipe(Recipe title) {
    textList.add(title);  
    notifyListeners();
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    var materialApp = MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Lukas und Jasmin\'s Wochenplaner :) '),
    );
    return ChangeNotifierProvider(
      create: (_) => StateRecipes(),
      child: materialApp,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  FlutterLocalNotificationsPlugin notif;

  _MyHomePageState()
      : notif = FlutterLocalNotificationsPlugin(),
        super() {
    notif.initialize(
        InitializationSettings(
          android: const AndroidInitializationSettings("appIcon"),
          linux: LinuxInitializationSettings(
              defaultActionName: "Okay :)",
              defaultIcon: ThemeLinuxIcon("dialog-information")),
        ),
        onDidReceiveNotificationResponse: (resp) => {
              switch (resp.actionId) {
                "reset" => setState(() => _counter = 0),
                _ => Logger().w("unexpected action: $resp.actionId"),
              }
            });
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
    notif.show(
      1,
      "Test!!",
      "The current counter is $_counter",
      const NotificationDetails(
        linux: LinuxNotificationDetails(
          urgency: LinuxNotificationUrgency.critical,
          actions: [
            LinuxNotificationAction(key: "foo", label: "Foo :)"),
            LinuxNotificationAction(key: "reset", label: "Reset!!!"),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
        actions: <Widget>[
          PopupMenuButton(
            itemBuilder: (ctx) => [
              const PopupMenuItem(child: Text("test :)))")),
              const PopupMenuItem(child: Text("test2…")),
            ],
          )
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const Text(
            'You have pushed the button this many times:',
          ),
          Text(
            '$_counter',
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          Expanded(child: recipesList())
        ],
      ),
      floatingActionButton: Consumer<StateRecipes>(
        builder: (_, recipes, __) => FloatingActionButton(
          onPressed: () => recipes.addRecipe("Hihihihihi :)"),
          tooltip: 'Increment',
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}

Consumer<StateRecipes> recipesList() {
  return Consumer<StateRecipes>(
    builder: (context, state, _) => ListView(
        children: state.textList
            .map((l) => recipeCard(context, title: l, description: l))
            .toList()),
  );
}

// TODO use recipe record
Card recipeCard(
  BuildContext context, {
  required String title,
  required String description,
}) {
  return Card(
    color: Theme.of(context).colorScheme.tertiaryContainer,
    child: Center(
      child: Column(
        children: [
          ListTile(
            title: Text(
              title,
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ),
          ListTile(title: Text(description)),
        ],
      ),
    ),
  );
}
